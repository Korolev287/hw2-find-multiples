// Циклы нужны для того, чтобы проделать одно и тоже действие несколько раз и уменьшить объем файла при написании кода.


const userNumber = prompt("Укажите число!");

if (userNumber < 5) {
    alert("Sorry, no numbers");
} else {
    for (let i = 0; i <= userNumber; i++) {
        if (i % 5 == 0) {
            console.log(i);
        }
    }
}

